/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code TagService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static final String EXTRAS_USER_ID = "USER_ID";
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    private TextView mConnectionState;
    private String mDeviceName;
    private String mUserId;
    private String mDeviceAddress;
    private Button mEnterButton;
    private Button mExitButton;
    private Button mDumpButton;
    private HashMap<UUID, BluetoothGattCharacteristic> mGattCharacteristics;
    private boolean mConnected = false;

    private TagServiceConnection mServiceConnection;

    private TagService getService() {
        return mServiceConnection.getService();
    }

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (TagService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            }
            else if (TagService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
            }
            else if (TagService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                mGattCharacteristics = getService().getTagServiceGattCharacteristics();
                if (getService().employeeAdmin(mUserId)) {
                    mDumpButton.setVisibility(View.VISIBLE);
                }
                else {
                    mDumpButton.setVisibility(View.INVISIBLE);
                }
            }
            else if (TagService.ACTION_DATA_AVAILABLE.equals(action)) {
                // Do something?
            }
            else if (TagReturnCodes.TAG_SUCCESS == action) {
                Toast.makeText(getApplicationContext(),
                        "Operation completed successfully", Toast.LENGTH_LONG).show();
            }
        }
    };

    private void writeDumpFile(final byte[] data) {
        final String filename = "dump.bin";
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File (sdCard.getAbsolutePath() + "/TagLessApp/");
        dir.mkdirs();
        File outputFile = new File(dir, filename);
        final String outputFilePath = outputFile.getAbsolutePath();


        if (ContextCompat.checkSelfPermission(this, // request permission when it is not granted.
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }

        
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(outputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    String.format("Could not create file %s", outputFilePath),
                    Toast.LENGTH_LONG).show();
            return;
        }

        try {
            outputStream.write(data);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    String.format("Could not save file %s", outputFilePath),
                    Toast.LENGTH_LONG).show();
        }
    }

    private final View.OnClickListener enterButtonOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        getService().sendUserIdWithTimestamp(TagGattAttributes.TAG_ENTER, mUserId);
                    } catch (TimeoutException e) {
                        Toast.makeText(getApplicationContext(),
                                String.format("Operation timed out"),
                                Toast.LENGTH_LONG).show();
                    }
                }
    };

    private final View.OnClickListener exitButtonOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        getService().sendUserIdWithTimestamp(TagGattAttributes.TAG_EXIT, mUserId);
                    } catch (TimeoutException e) {
                        Toast.makeText(getApplicationContext(),
                                String.format("Operation timed out"),
                                Toast.LENGTH_LONG).show();
                    }
                }
            };

    private final View.OnClickListener dumpButtonOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        byte[] data = getService().getFullDump();
                        writeDumpFile(data);
                        Toast.makeText(getApplicationContext(),
                                "Dump success", Toast.LENGTH_LONG).show();
                    } catch (TimeoutException e) {
                        Toast.makeText(getApplicationContext(),
                                "Communication Error", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(),
                                "IO Error", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        mUserId = intent.getStringExtra(EXTRAS_USER_ID);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        ((TextView) findViewById(R.id.id_value)).setText(mUserId);

        // Bind buttons
        mEnterButton = (Button) findViewById(R.id.enter);
        mEnterButton.setOnClickListener(enterButtonOnClickListener);

        mExitButton = (Button) findViewById(R.id.exit);
        mExitButton.setOnClickListener(exitButtonOnClickListener);

        mDumpButton = (Button) findViewById(R.id.dump);
        mDumpButton.setOnClickListener(dumpButtonOnClickListener);

        mConnectionState = (TextView) findViewById(R.id.connection_state);

        mServiceConnection = new TagServiceConnection(mDeviceAddress);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent gattServiceIntent = new Intent(this, TagService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (getService() != null) {
            final boolean result = getService().connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_connect:
                getService().connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                getService().disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TagService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(TagService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(TagService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(TagService.ACTION_DATA_AVAILABLE);
        // for some reason, adding the following in a loop causes the app to crash
        intentFilter.addAction(TagReturnCodes.TAG_SUCCESS);
        intentFilter.addAction(TagReturnCodes.INCONSISTENT_RECORDS);
        intentFilter.addAction(TagReturnCodes.NO_EMPLOYEE);

        return intentFilter;
    }
}
