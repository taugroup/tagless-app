/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import java.util.HashSet;
import java.util.UUID;

/**
 * This class includes the gatt attributes of the tagless app
 */
public class TagGattAttributes {
    public static UUID TAG_ENTER = UUID.fromString("f0001141-0451-4000-b000-000000000000");
    public static UUID TAG_EXIT = UUID.fromString("f0001142-0451-4000-b000-000000000000");
    public static UUID TAG_DUMP = UUID.fromString("f0001143-0451-4000-b000-000000000000");
    public static UUID TAG_COUNT = UUID.fromString("f0001144-0451-4000-b000-000000000000");
    public static UUID TAG_CHUNK = UUID.fromString("f0001145-0451-4000-b000-000000000000");
    public static UUID TAG_ADMIN = UUID.fromString("f0001146-0451-4000-b000-000000000000");
    public static UUID TAG_EXISTS = UUID.fromString("f0001147-0451-4000-b000-000000000000");

    public static HashSet<UUID> TAG_ALL_ATTRIBUTES = new HashSet<UUID>();
    static {
        TAG_ALL_ATTRIBUTES.add(TAG_ENTER);
        TAG_ALL_ATTRIBUTES.add(TAG_EXIT);
        TAG_ALL_ATTRIBUTES.add(TAG_DUMP);
        TAG_ALL_ATTRIBUTES.add(TAG_COUNT);
        TAG_ALL_ATTRIBUTES.add(TAG_CHUNK);
        TAG_ALL_ATTRIBUTES.add(TAG_ADMIN);
        TAG_ALL_ATTRIBUTES.add(TAG_EXISTS);
    }
}
