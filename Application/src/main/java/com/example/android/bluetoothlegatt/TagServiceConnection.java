package com.example.android.bluetoothlegatt;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Ynon on 30/03/2017.
 */

public class TagServiceConnection implements ServiceConnection {

    private TagService mTagService;
    private String mDeviceAddress;

    public TagServiceConnection(String deviceAddress) {
        mDeviceAddress = deviceAddress;
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service) {
        mTagService = ((TagService.LocalBinder) service).getService();
        if (!mTagService.initialize()) {
            Log.e("TagServiceConnection", "Unable to initialize Bluetooth");
        }
        // Automatically connects to the device upon successful start-up initialization.
        mTagService.connect(mDeviceAddress);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        mTagService.disconnect();
        mTagService = null;
    }


    public TagService getService() {
        return mTagService;
    }
}
