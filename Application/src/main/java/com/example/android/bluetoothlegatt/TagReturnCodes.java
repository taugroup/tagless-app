package com.example.android.bluetoothlegatt;

import java.util.HashMap;

/**
 * Created by Ynon on 29/03/2017.
 */

public class TagReturnCodes {

    public final static String TAG_SUCCESS =
            "com.example.bluetooth.le.TAG_SUCCESS";
    public final static String NO_EMPLOYEE =
            "com.example.bluetooth.le.NO_EMPLOYEE";
    public final static String INCONSISTENT_RECORDS =
            "com.example.bluetooth.le.INCONSISTENT_RECORDS";

    public final static HashMap<Integer, String> codesToNotifications =
            new HashMap<Integer, String>();

    static {
        codesToNotifications.put(
                ServerReturnValues.TAG_SUCCESS.get(), TAG_SUCCESS);
        codesToNotifications.put(
                ServerReturnValues.NO_EMPLOYEE.get(), NO_EMPLOYEE);
        codesToNotifications.put(
                ServerReturnValues.INCONSISTENT_RECORDS.get(), INCONSISTENT_RECORDS);
    }

    public enum ServerReturnValues {
        TAG_SUCCESS(0),
        INCONSISTENT_RECORDS(4),
        NO_EMPLOYEE(5),
        NO_RECORDS(6),
        NOT_ADMIN(9);

        private int val;

        ServerReturnValues(int val) {
            this.val = val;
        }

        int get() {
            return val;
        }
    }

}
