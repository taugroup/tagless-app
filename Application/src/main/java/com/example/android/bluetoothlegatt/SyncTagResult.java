package com.example.android.bluetoothlegatt;

/**
 * Created by Ynon on 01/04/2017.
 */

public class SyncTagResult {

    private int status;
    private byte[] bytes;

    public SyncTagResult(int status, byte[] bytes) {
        this.status = status;
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public int getStatus() {
        return status;
    }
}
