package com.example.android.bluetoothlegatt;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.android.bluetoothlegatt.DeviceControlActivity.EXTRAS_DEVICE_ADDRESS;
import static com.example.android.bluetoothlegatt.DeviceControlActivity.EXTRAS_DEVICE_NAME;
import static com.example.android.bluetoothlegatt.DeviceControlActivity.EXTRAS_USER_ID;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private String mDeviceName;
    private String mDeviceAddress;
    private TagServiceConnection mServiceConnection;

    final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (TagService.ACTION_GATT_CONNECTED.equals(action)) {
                Toast.makeText(getApplicationContext(),
                        "BLE Connection established", Toast.LENGTH_LONG).show();
                showProgress(false);
            }
            else if (TagService.ACTION_GATT_DISCONNECTED.equals(action)) {
                Toast.makeText(getApplicationContext(),
                        "BLE disconnected ", Toast.LENGTH_LONG).show();
            }
            else if (TagReturnCodes.NO_EMPLOYEE.equals(action)) {
                Toast.makeText(getApplicationContext(),
                        "User does not exist", Toast.LENGTH_LONG).show();
            }
            else if (TagService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

            }
            else if (TagService.ACTION_DATA_AVAILABLE.equals(action)) {

            }
            else if (TagReturnCodes.TAG_SUCCESS == action) {
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        mServiceConnection = new TagServiceConnection(mDeviceAddress);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        Intent gattServiceIntent = new Intent(this, TagService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        showProgress(true);
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (getService() != null) {
            final boolean result = getService().connect(mDeviceAddress);
            Log.d("LoginActivity", "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
    }

    private TagService getService() {
        return mServiceConnection.getService();
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TagService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(TagService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(TagService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(TagService.ACTION_DATA_AVAILABLE);
        // for some reason, adding the following in a loop causes the app to crash
        intentFilter.addAction(TagReturnCodes.TAG_SUCCESS);
        intentFilter.addAction(TagReturnCodes.INCONSISTENT_RECORDS);
        intentFilter.addAction(TagReturnCodes.NO_EMPLOYEE);

        return intentFilter;
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!isUserIdValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_id));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isUserIdValid(String userId) {
        final int validUserIdLength = 4;
        final int userIdLength = userId.length();
        if (userIdLength != validUserIdLength) {
            Toast.makeText(getApplicationContext(),
                    String.format("ID should be %d characters long", validUserIdLength),
                    Toast.LENGTH_LONG).show();
            return false;
        }

        try {
            Integer.parseInt(userId);
        }
        catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(),
                    String.format("ID %s is not a valid integer", userId),
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mPassword;

        UserLoginTask(String password) {
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return getService().employeeExists(mPassword);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                final Intent intent = new Intent(LoginActivity.this,
                        DeviceControlActivity.class);
                intent.putExtra(EXTRAS_DEVICE_NAME, mDeviceName);
                intent.putExtra(EXTRAS_DEVICE_ADDRESS, mDeviceAddress);
                intent.putExtra(EXTRAS_USER_ID, mPassword);
                startActivity(intent);
            } else {
                mPasswordView.setError(getString(R.string.error_invalid_id));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

